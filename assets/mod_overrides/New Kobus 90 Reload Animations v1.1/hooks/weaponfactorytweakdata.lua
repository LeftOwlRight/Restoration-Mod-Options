Hooks:PostHook( WeaponFactoryTweakData, "init", "PBP90ModInit", function(self)


	self.parts.wpn_fps_smg_p90_m_strap.animations = nil
		
	self.parts.wpn_fps_smg_p90_m_std.animations = nil
		
	if self.parts.wpn_fps_smg_p90_m_chicken then
		self.parts.wpn_fps_smg_p90_m_chicken.animations = nil
	end
	
end)