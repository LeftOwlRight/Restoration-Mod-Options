local old_weapon_tweak_data = NewRaycastWeaponBase.weapon_tweak_data
function NewRaycastWeaponBase:weapon_tweak_data(...)
    local weapon_tweak_data = old_weapon_tweak_data(self, ...)

    if not self._parts or self._name_id ~= "colt_1911" then
        return weapon_tweak_data
    end

	if SystemFS:exists("assets/mod_overrides/One-Handed Pistol Animations/anims/fps/glock/glock_idle.animation") then
		return weapon_tweak_data
	end

	weapon_tweak_data.weapon_hold = "crosskillnew"

	if self._parts.wpn_fps_pis_1911_m_standard then
		weapon_tweak_data.animations.reload_name_id = "crosskillnew"
	end

	if self._parts.wpn_fps_pis_1911_m_extended or self._parts.wpn_fps_pis_1911_m_big then
		weapon_tweak_data.animations.reload_name_id = "crosskillnewext"
	end

    return weapon_tweak_data
end