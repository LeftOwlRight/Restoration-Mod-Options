local old_weapon_tweak_data = NewRaycastWeaponBase.weapon_tweak_data
function NewRaycastWeaponBase:weapon_tweak_data(...)
    local weapon_tweak_data = old_weapon_tweak_data(self, ...)

    if not self._parts or self._name_id ~= "fal" then
        return weapon_tweak_data 
    end
	
	if SC then
		if self._parts.wpn_fps_ass_fal_m_standard then 
			weapon_tweak_data.animations.reload_name_id = "falext" end
		if self._parts.wpn_fps_ass_fal_m_01 then 
			weapon_tweak_data.animations.reload_name_id = "fal" end
	else
		if self._parts.wpn_fps_ass_fal_m_standard then 
			weapon_tweak_data.animations.reload_name_id = "fal" end
		if self._parts.wpn_fps_ass_fal_m_01 then 
			weapon_tweak_data.animations.reload_name_id = "falext" end
		if self._parts.wpn_fps_ass_fal_m_magpul then 
			weapon_tweak_data.animations.reload_name_id = "fal" end
		if self._parts.wpn_fps_ass_fal_m_quick then 
			weapon_tweak_data.animations.reload_name_id = "fal" end
	end

    return weapon_tweak_data
end