Hooks:PostHook( WeaponFactoryTweakData, "init", "PBFalModInit", function(self)

	self.wpn_fps_ass_fal.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}

	self.parts.wpn_fps_ass_fal_m_standard.animations = nil
	self.parts.wpn_fps_ass_fal_m_01.animations = nil
	
	
	if self.parts.wpn_fps_ass_fal_m_magpul then
		self.parts.wpn_fps_ass_fal_m_magpul.animations = nil
	end
	
	if self.parts.wpn_fps_ass_fal_m_quick then
		self.parts.wpn_fps_ass_fal_m_quick.animations = nil
	end


end)