Hooks:PostHook( WeaponFactoryTweakData, "init", "PBScarHModInit", function(self)

	self.wpn_fps_ass_scar.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}

	self.parts.wpn_fps_ass_scar_m_standard.animations = nil
	
	
	if self.parts.wpn_fps_upg_m_mk17 then
		self.parts.wpn_fps_upg_m_mk17.animations = nil
	end

	if self.parts.wpn_fps_ass_scar_m_magplate then
		self.parts.wpn_fps_ass_scar_m_magplate.animations = nil
	end

	if self.parts.wpn_fps_ass_scar_m_full then
		self.parts.wpn_fps_ass_scar_m_full.animations = nil
	end
	
	if self.parts.wpn_fps_ass_scar_m_ssr then
		self.parts.wpn_fps_ass_scar_m_ssr.animations = nil
	end




	if self.parts.wpn_fps_ass_scar_m_extended then
		self.parts.wpn_fps_ass_scar_m_extended.animations = nil
		
	end
	if self.parts.wpn_fps_ass_scar_m_extended_strap then
		self.parts.wpn_fps_ass_scar_m_extended_strap.animations = nil
	end

	if self.parts.wpn_fps_ass_scar_m_extended_black then
		self.parts.wpn_fps_ass_scar_m_extended_black.animations = nil
	end

	if self.parts.wpn_fps_ass_scar_m_extended_strap_black then
		self.parts.wpn_fps_ass_scar_m_extended_strap_black.animations = nil
	end


end)