Hooks:PostHook( WeaponFactoryTweakData, "init", "PBG36ModInit", function(self)

	self.wpn_fps_ass_g36.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}

	self.parts.wpn_fps_ass_g36_m_standard.animations = nil
	self.parts.wpn_fps_ass_g36_m_quick.animations = nil
	
	
	if self.parts.wpn_fps_upg_m_g36_dura then
		self.parts.wpn_fps_upg_m_g36_dura.animations = nil
	end
	
	if self.parts.wpn_fps_upg_m_g36_d60 then
		self.parts.wpn_fps_upg_m_g36_d60.animations = nil
	end

	if self.parts.wpn_fps_upg_m_g36_p30 then
		self.parts.wpn_fps_upg_m_g36_p30.animations = nil
	end

	if self.parts.wpn_fps_upg_m_g36_p30w then
		self.parts.wpn_fps_upg_m_g36_p30w.animations = nil
	end
	
	if self.parts.wpn_fps_upg_m_g36_p40 then
		self.parts.wpn_fps_upg_m_g36_p40.animations = nil
	end
	
	if self.parts.wpn_fps_upg_m_g36_poly then
		self.parts.wpn_fps_upg_m_g36_poly.animations = nil
	end
	
	if self.parts.wpn_fps_upg_m_g36_battle then
		self.parts.wpn_fps_upg_m_g36_battle.animations = nil
	end
	
	if self.parts.wpn_fps_upg_m_g36_hksteel then
		self.parts.wpn_fps_upg_m_g36_hksteel.animations = nil
	end
	
	if self.parts.wpn_fps_upg_m_g36_gen2 then
		self.parts.wpn_fps_upg_m_g36_gen2.animations = nil
	end
	
	if self.parts.wpn_fps_upg_m_g36_lanc then 
		self.parts.wpn_fps_upg_m_g36_lanc.animations = nil
	end
	
	
	if self.parts.wpn_fps_upg_m_g36_dmmag then 
		self.parts.wpn_fps_upg_m_g36_dmmag.animations = nil
	end
	
	if self.parts.wpn_fps_upg_m_g36_lanct then 
		self.parts.wpn_fps_upg_m_g36_lanct.animations = nil
	end
		--jesus, frenchy
end)