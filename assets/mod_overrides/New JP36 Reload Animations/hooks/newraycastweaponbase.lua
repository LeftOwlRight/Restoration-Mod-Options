local old_weapon_tweak_data = NewRaycastWeaponBase.weapon_tweak_data
function NewRaycastWeaponBase:weapon_tweak_data(...)
    local weapon_tweak_data = old_weapon_tweak_data(self, ...)

    if not self._parts or self._name_id ~= "g36" then
        return weapon_tweak_data 
    end
	

	if self._parts.wpn_fps_ass_g36_gl then 
		weapon_tweak_data.animations.reload_name_id = "g36ub"
	else
		weapon_tweak_data.animations.reload_name_id = "g36" end


    return weapon_tweak_data
end