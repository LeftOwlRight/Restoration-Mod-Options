local old_weapon_tweak_data = NewRaycastWeaponBase.weapon_tweak_data
function NewRaycastWeaponBase:weapon_tweak_data(...)
    local weapon_tweak_data = old_weapon_tweak_data(self, ...)

    if not self._parts or self._name_id ~= "deagle" then
        return weapon_tweak_data 
    end
	
	if SystemFS:exists("assets/mod_overrides/One-Handed Pistol Animations/anims/fps/glock/glock_idle.animation") then
		if self._parts.wpn_fps_pis_deagle_m_extended then 
			weapon_tweak_data.animations.reload_name_id = "deagleohext"
		else
			weapon_tweak_data.animations.reload_name_id = "deagleoh" end
	else
		if self._parts.wpn_fps_pis_deagle_m_extended then 
			weapon_tweak_data.animations.reload_name_id = "deagleext"
		else
			weapon_tweak_data.animations.reload_name_id = "deagle" end
	end

    return weapon_tweak_data
end