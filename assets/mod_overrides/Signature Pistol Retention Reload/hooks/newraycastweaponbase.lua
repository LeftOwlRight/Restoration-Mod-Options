local old_weapon_tweak_data = NewRaycastWeaponBase.weapon_tweak_data
function NewRaycastWeaponBase:weapon_tweak_data(...)
    local weapon_tweak_data = old_weapon_tweak_data(self, ...)

    if not self._parts or self._name_id ~= "p226" then
        return weapon_tweak_data 
    end
	

	if self._parts.wpn_fps_pis_p226_m_standard then 
		weapon_tweak_data.animations.reload_name_id = "p226ret" end
		
	if self._parts.wpn_fps_pis_p226_m_extended then 
		weapon_tweak_data.animations.reload_name_id = "p226ext" end


    return weapon_tweak_data
end