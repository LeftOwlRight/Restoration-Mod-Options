Hooks:PostHook( WeaponFactoryTweakData, "init", "PBSigP226RetModInit", function(self)


	self.parts.wpn_fps_pis_p226_m_standard.reload_objects = {
        reload_not_empty = "g_spare_bullets",
        reload = "g_spare_bullets"
        }
		
	self.parts.wpn_fps_pis_p226_m_extended.reload_objects = {
        reload_not_empty = "g_spare_bullets",
        reload = "g_spare_bullets"
        }
		
	if self.parts.wpn_fps_pis_p226_m_standard then
		self.parts.wpn_fps_pis_p226_m_standard.animations = {
			reload_not_empty = "reload_not_empty",
			reload = "reload"
		}
	end
	
	if self.parts.wpn_fps_pis_p226_m_extended then
		self.parts.wpn_fps_pis_p226_m_extended.animations = {
			reload_not_empty = "reload_not_empty",
			reload = "reload"
		}
	end
	
end)