Hooks:PostHook(WeaponFactoryTweakData, "create_bonuses", "WinchesterM1894ModInit", function(self)
	if self.parts.wpn_fps_snp_winchester1894_irons and self.parts.wpn_fps_upg_winchester1894_irons_ilu then
		table.list_append(self.parts.wpn_fps_snp_winchester1894_irons.forbids, {"wpn_fps_upg_o_45iron","wpn_fps_snp_winchester1894_sight_rail"})
		table.list_append(self.parts.wpn_fps_upg_winchester1894_irons_ilu.forbids, {"wpn_fps_upg_o_45iron","wpn_fps_snp_winchester1894_sight_rail"})
	end
	local custom_wpn_id = "wpn_fps_snp_winchester1894"
	local stance_wpn_id = "wpn_fps_snp_tti"
	for _, part_id in pairs(self[custom_wpn_id].uses_parts) do
		if self.parts[part_id].type == "sight" or self.parts[part_id].type == "gadget" then
			if self.parts[part_id].stance_mod and self.parts[part_id].stance_mod[stance_wpn_id] then
				self.parts[part_id].stance_mod[custom_wpn_id] = deep_clone(self.parts[part_id].stance_mod[stance_wpn_id])
			end
		end
	end
end)