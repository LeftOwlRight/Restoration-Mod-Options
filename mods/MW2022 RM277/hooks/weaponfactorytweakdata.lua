Hooks:PostHook(WeaponFactoryTweakData, "init", "mw2022_ngsierra_weaponfactorytweakdata_init", function(self)
--  NGSW Optic
	local custom_sight_id = "wpn_fps_ass_ngsierra_optic_ngsw"
	local based_on_id = "wpn_fps_upg_o_specter"
	local offset = Vector3(0, 7, 1.70)
	if self.parts[custom_sight_id].stance_mod then
		for _, wpn_id in pairs(self.parts[custom_sight_id].stance_mod) do
			wpn_id.translation = (wpn_id.translation + offset)
		end
	end
	
	
	for _, part_id in pairs(self.parts) do
		if part_id.override and part_id.override[based_on_id] then
			part_id.override[custom_sight_id] = deep_clone(part_id.override[based_on_id])
			if part_id.override[custom_sight_id].stance_mod then
			for _, wpn_id in pairs(part_id.override[custom_sight_id].stance_mod) do
					wpn_id.translation = (wpn_id.translation + offset)
				end
			end
		end
	end 
	
--	2042 Angled Irons
	self.parts.wpn_fps_ass_ngsierra_irons_angled.stance_mod.wpn_fps_ass_ngsierra = {
            translation = Vector3(-1.8, -13, -11.3),
             rotation = Rotation(-0.02, 0.04, -45)
            }



--	poopy method for enabling attachment anims
	if self.parts.wpn_fps_ass_ngsierra_magazine then
		self.parts.wpn_fps_ass_ngsierra_magazine.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}
	end
		
	if self.parts.wpn_fps_ass_ngsierra_xmag then
		self.parts.wpn_fps_ass_ngsierra_xmag.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}
	end
	
	if self.parts.wpn_fps_ass_ngsierra_charging_handle then
		self.parts.wpn_fps_ass_ngsierra_charging_handle.animations = {
		reload = "reload"
		}
	end

end)