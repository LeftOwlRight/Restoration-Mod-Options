Hooks:PostHook(WeaponFactoryTweakData, "init", "bocw_lc10_weaponfactorytweakdata_init", function(self)
	
	self.parts.wpn_fps_smg_lc10_magazine.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
	}
	self.parts.wpn_fps_smg_lc10_magazine_fast_01.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
	}
	self.parts.wpn_fps_smg_lc10_magazine_fast_01_pro.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
	}
	self.parts.wpn_fps_smg_lc10_magazine_mix_01.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
	}
	self.parts.wpn_fps_smg_lc10_magazine_mix_01_pro.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
	}
	self.parts.wpn_fps_smg_lc10_xmag_01.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
	}
	self.parts.wpn_fps_smg_lc10_xmag_01_pro.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
	}

	
end)

