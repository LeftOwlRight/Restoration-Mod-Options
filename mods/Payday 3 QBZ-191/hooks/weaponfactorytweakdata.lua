Hooks:PostHook(WeaponFactoryTweakData, "init", "pd3_qbz191_weaponfactorytweakdata_init", function(self)

 self.parts.wpn_fps_ass_pd3_qbz191_magazine.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}
 self.parts.wpn_fps_ass_pd3_qbz191_magazine_quick.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}
 self.parts.wpn_fps_ass_pd3_qbz191_smag.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
	}		
 self.parts.wpn_fps_ass_pd3_qbz191_xmag.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
	}

 self.parts.wpn_fps_ass_pd3_qbz191_receiver.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}
		
end)