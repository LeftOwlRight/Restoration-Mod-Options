{
	"bm_w_wmtx" : "Widowmaker TX",
	"bm_wp_wpn_fps_upg_wmtx_gastube_burst" : "Rapid Fire System",
	"bm_wp_wpn_fps_upg_wmtx_gastube_burst_desc" : "Enables fully-automatic fire mode.",
	"bm_wp_wpn_fps_upg_wmtx_heatshield" : "Heat Shield",
	"bm_wp_wpn_fps_upg_wmtx_ns_firebull" : "Firebull Flash Hider",

	"menu_l_global_value_wmtxhrmod" : "This is a >:3's mod!"
}