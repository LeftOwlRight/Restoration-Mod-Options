Hooks:PostHook(WeaponFactoryTweakData, "create_bonuses", "G3HK79ModInit", function(self)
	local custom_wpn_id = "wpn_fps_ass_g3hk79"
	local offset = Vector3(1.425, 0, 0.5)
	for _, part_id in pairs(self.parts) do
		if part_id.type == "second_sight" and part_id.a_obj == "a_o" and part_id.stance_mod[custom_wpn_id] then
			if part_id.stance_mod[custom_wpn_id].translation and not part_id.stance_mod[custom_wpn_id].offset_applied then
				part_id.stance_mod[custom_wpn_id].translation = (part_id.stance_mod[custom_wpn_id].translation + offset)
				part_id.stance_mod[custom_wpn_id].offset_applied = true
			end
		end
	end
	self.parts.wpn_fps_upg_a_underbarrel_hornet.override.wpn_fps_ass_g3hk79_gl = {unit = "units/mods/weapons/wpn_fps_ass_g3hk79_pts/wpn_fps_ass_g3hk79_gl_hornet", muzzleflash = "effects/payday2/particles/weapons/shotgun/sho_muzzleflash_hornet"}
end)