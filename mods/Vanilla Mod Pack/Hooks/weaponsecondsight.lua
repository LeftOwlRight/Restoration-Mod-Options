WeaponLaserSecondSight = WeaponLaserSecondSight or class(WeaponLaser)

function WeaponLaserSecondSight:init(unit)
	WeaponLaserSecondSight.super.init(self, unit)
	WeaponSecondSight.init(self, unit)
end

function WeaponLaserSecondSight:_check_state(current_state)
	WeaponLaserSecondSight.super._check_state(self, current_state)
	WeaponSecondSight._check_state(self, current_state)
end

WeaponLaserSecondSight.toggle_requires_stance_update = WeaponSecondSight.toggle_requires_stance_update
WeaponLaserSecondSight.play_anim = WeaponSecondSight.play_anim