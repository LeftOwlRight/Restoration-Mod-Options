local old_weapon_tweak_data = NewRaycastWeaponBase.weapon_tweak_data
function NewRaycastWeaponBase:weapon_tweak_data(...)
    local weapon_tweak_data = old_weapon_tweak_data(self, ...)

    if not self._parts or self._name_id ~= "swhiskey" then
        return weapon_tweak_data 
    end
	

	if self._parts.wpn_fps_pis_swhiskey_animation_default then 
		weapon_tweak_data.animations.reload_name_id = "swhiskey" end
		
	if self._parts.wpn_fps_pis_swhiskey_animation_spin then 
		weapon_tweak_data.animations.reload_name_id = "swhiskeyspin" end
				
	if self._parts.wpn_fps_pis_swhiskey_animation_tacticool then 
		weapon_tweak_data.animations.reload_name_id = "swhiskeytac" end
				
	if self._parts.wpn_fps_pis_swhiskey_animation_gaming then 
		weapon_tweak_data.animations.reload_name_id = "swhiskeygaming" end


    return weapon_tweak_data
end