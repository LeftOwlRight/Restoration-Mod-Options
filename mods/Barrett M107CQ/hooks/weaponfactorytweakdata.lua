Hooks:PostHook(WeaponFactoryTweakData, "create_bonuses", "M107CQModInit", function(self)
	if self.parts.wpn_fps_upg_m107cq_iron_sights then
		table.list_append(self.parts.wpn_fps_upg_m107cq_iron_sights.forbids, {"wpn_fps_snp_m107cq_iron_sights_down"})
	end
	local custom_wpn_id = "wpn_fps_snp_m107cq"
	local offset = Vector3(-0.075, 5, 0.075)
	for _, part_id in pairs(self.parts) do
		if part_id.type == "gadget" and part_id.sub_type == "second_sight" and part_id.stance_mod and part_id.a_obj == "a_o" then
			if part_id.stance_mod[custom_wpn_id] and part_id.stance_mod[custom_wpn_id].translation then
				part_id.stance_mod[custom_wpn_id].translation = (part_id.stance_mod[custom_wpn_id].translation + offset)
			end
		end
	end
end)