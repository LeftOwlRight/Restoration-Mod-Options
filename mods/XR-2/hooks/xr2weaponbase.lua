XR2WeaponBase = XR2WeaponBase or class(NewRaycastWeaponBase)

function XR2WeaponBase:clbk_assembly_complete(...)
	XR2WeaponBase.super.clbk_assembly_complete(self, ...)

	if table.contains(self._blueprint, "wpn_fps_upg_xr2_mag_ext_01") or table.contains(self._blueprint, "wpn_fps_upg_xr2_mag_ext_02") then
		self:weapon_tweak_data().animations.reload_name_id = "xr2_longmag"
	end
end