Hooks:PostHook(WeaponFactoryTweakData, "init", "mcx_spear_custom_stats_init", function(self)

--  NGSW Optic
	local custom_sight_id = "wpn_fps_ass_mcx_spear_optic_ngsw"
	local based_on_id = "wpn_fps_upg_o_specter"
	local offset = Vector3(0, 6, 1.6)
	if self.parts[custom_sight_id].stance_mod then
		for _, wpn_id in pairs(self.parts[custom_sight_id].stance_mod) do
			wpn_id.translation = (wpn_id.translation + offset)
	end
	end
	for _, part_id in pairs(self.parts) do
		if part_id.override and part_id.override[based_on_id] then
			part_id.override[custom_sight_id] = deep_clone(part_id.override[based_on_id])
			if part_id.override[custom_sight_id].stance_mod then
			for _, wpn_id in pairs(part_id.override[custom_sight_id].stance_mod) do
					wpn_id.translation = (wpn_id.translation + offset)
				end
			end
		end
	end 

--  NGSW Optic with Remote
	local custom_sight_id = "wpn_fps_ass_mcx_spear_optic_ngsw_remote"
	local based_on_id = "wpn_fps_upg_o_specter"
	local offset = Vector3(0, 6, 1.6)
	if self.parts[custom_sight_id].stance_mod then
		for _, wpn_id in pairs(self.parts[custom_sight_id].stance_mod) do
			wpn_id.translation = (wpn_id.translation + offset)
	end
	end
	for _, part_id in pairs(self.parts) do
		if part_id.override and part_id.override[based_on_id] then
			part_id.override[custom_sight_id] = deep_clone(part_id.override[based_on_id])
			if part_id.override[custom_sight_id].stance_mod then
			for _, wpn_id in pairs(part_id.override[custom_sight_id].stance_mod) do
					wpn_id.translation = (wpn_id.translation + offset)
				end
			end
		end
	end 



	if BeardLib.Utils:FindMod("Tacticool Stocks") then
		local weapon_id = "wpn_fps_ass_mcx_spear"
		local weapon_uses_parts_list = {
										"wpn_fps_upg_mpx_s_maxim",
										"wpn_fps_upg_mpx_s_ulss",
										"wpn_fps_upg_mpx_s_thin",
										"wpn_fps_upg_mpx_s_tele",
										"wpn_fps_upg_mpx_s_collap"
										}
		for _,part_id in ipairs( weapon_uses_parts_list ) do
			if self[weapon_id] then
				table.insert( self[weapon_id].uses_parts, part_id )
			end
		end
	end			


--  NGSW Angled Irons
	self.parts.wpn_fps_ass_mcx_spear_irons_angled_ngsw.stance_mod.wpn_fps_ass_mcx_spear = {
            translation = Vector3(-1.2, -5, -8.75),
             rotation = Rotation(0, 0, -45)
            }
	
end)