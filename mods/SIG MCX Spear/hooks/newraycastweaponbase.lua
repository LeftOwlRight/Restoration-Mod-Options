local old_weapon_tweak_data = NewRaycastWeaponBase.weapon_tweak_data
function NewRaycastWeaponBase:weapon_tweak_data(...)
   local weapon_tweak_data = old_weapon_tweak_data(self, ...)

    if not self._parts or self._name_id ~= "mcx_spear" then
       return weapon_tweak_data 
   end
	
	if self._parts.wpn_fps_ass_mcx_spear_animation_default then 
		weapon_tweak_data.animations.reload_name_id = "mcx_spear_random" end
		
	if self._parts.wpn_fps_ass_mcx_spear_animation_ambi_bolt_release then 
		weapon_tweak_data.animations.reload_name_id = "mcx_spear_ambi" end
				
	if self._parts.wpn_fps_ass_mcx_spear_animation_bolt_release then 
		weapon_tweak_data.animations.reload_name_id = "mcx_spear_slap" end
				
	if self._parts.wpn_fps_ass_mcx_spear_animation_charging_handle then 
		weapon_tweak_data.animations.reload_name_id = "mcx_spear" end


   return weapon_tweak_data
end