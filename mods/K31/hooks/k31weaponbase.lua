K31WeaponBase = K31WeaponBase or class(NewRaycastWeaponBase)

function K31WeaponBase:mag_is_empty()
	self._mag_is_empty = self:clip_empty()

	return self._mag_is_empty
end

function K31WeaponBase:reload_expire_t()
	if self._use_shotgun_reload and not self:mag_is_empty() then
		local ammo_remaining_in_clip = self:get_ammo_remaining_in_clip()

		return math.min(self:get_ammo_total() - ammo_remaining_in_clip, self:get_ammo_max_per_clip() - ammo_remaining_in_clip) * self:reload_shell_expire_t()
	end

	return nil
end

function K31WeaponBase:reload_enter_expire_t()
	if self._use_shotgun_reload and not self:mag_is_empty() then
		return self:weapon_tweak_data().timers.shotgun_reload_enter or 0.3
	end

	return nil
end

function K31WeaponBase:reload_exit_expire_t()
	if self._use_shotgun_reload and not self:mag_is_empty() then
		return self:weapon_tweak_data().timers.shotgun_reload_exit_empty or 0.7
	end

	return nil
end

function K31WeaponBase:reload_not_empty_exit_expire_t()
	if self._use_shotgun_reload and not self:mag_is_empty() then
		return self:weapon_tweak_data().timers.shotgun_reload_exit_not_empty or 0.3
	end

	return nil
end

function K31WeaponBase:reload_shell_expire_t()
	if self._use_shotgun_reload and not self:mag_is_empty() then
		return self:weapon_tweak_data().timers.shotgun_reload_shell or 0.5666666666666667
	end

	return nil
end

function K31WeaponBase:_first_shell_reload_expire_t()
	if self._use_shotgun_reload and not self:mag_is_empty() then
		return self:reload_shell_expire_t() - (self:weapon_tweak_data().timers.shotgun_reload_first_shell_offset or 0.33)
	end

	return nil
end

function K31WeaponBase:start_reload(...)
	if self._use_shotgun_reload and not self:mag_is_empty() then
		self._started_reload_empty = self:clip_empty()
		local speed_multiplier = self:reload_speed_multiplier()
		self._next_shell_reloded_t = managers.player:player_timer():time() + self:_first_shell_reload_expire_t() / speed_multiplier
		self._current_reload_speed_multiplier = speed_multiplier
	end
end

function K31WeaponBase:started_reload_empty()
	if self._use_shotgun_reload and not self:mag_is_empty() then
		return self._started_reload_empty
	end

	return nil
end

function K31WeaponBase:update_reloading(t, dt, time_left)
	if self._use_shotgun_reload and self._next_shell_reloded_t and self._next_shell_reloded_t < t and not self:mag_is_empty() then
		local speed_multiplier = self:reload_speed_multiplier()
		self._next_shell_reloded_t = self._next_shell_reloded_t + self:reload_shell_expire_t() / speed_multiplier

		self:set_ammo_remaining_in_clip(math.min(self:get_ammo_max_per_clip(), self:get_ammo_remaining_in_clip() + 1))
		managers.job:set_memory("kill_count_no_reload_" .. tostring(self._name_id), nil, true)

		return true
	end
end

function K31WeaponBase:reload_interuptable()
	if self._use_shotgun_reload and not self:mag_is_empty() then
		return true
	end

	return false
end