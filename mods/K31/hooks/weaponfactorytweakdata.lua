Hooks:PostHook(WeaponFactoryTweakData, "create_bonuses", "K31ModInit", function(self)
	if self.parts.wpn_fps_snp_k31_scope and self.parts.wpn_fps_upg_k31_rearsight and self.parts.wpn_fps_upg_k31_scope_zoom then
		table.list_append(self.parts.wpn_fps_snp_k31_scope.forbids, {"wpn_fps_snp_k31_scope_dummy","wpn_fps_snp_k31_sightrail","wpn_fps_upg_o_45iron"})
		table.list_append(self.parts.wpn_fps_upg_k31_rearsight.forbids, {"wpn_fps_snp_k31_sightrail","wpn_fps_upg_o_45iron"})
		table.list_append(self.parts.wpn_fps_upg_k31_scope_zoom.forbids, {"wpn_fps_snp_k31_scope_dummy","wpn_fps_snp_k31_sightrail","wpn_fps_upg_o_45iron"})
	end
	local custom_wpn_id = "wpn_fps_snp_k31"
	local offset = Vector3(0.675, 3, -2.15)
	for _, part_id in pairs(self.parts) do
		if part_id.type == "second_sight" and part_id.a_obj == "a_o" and part_id.stance_mod[custom_wpn_id] then
			if part_id.stance_mod[custom_wpn_id].translation and not part_id.stance_mod[custom_wpn_id].offset_applied then
				part_id.stance_mod[custom_wpn_id].translation = (part_id.stance_mod[custom_wpn_id].translation + offset)
				part_id.stance_mod[custom_wpn_id].offset_applied = true
			end
		end
	end
end)