Hooks:PostHook(WeaponFactoryTweakData, "create_bonuses", "Moss464SPXModInit", function(self)
	if self.parts.wpn_fps_snp_moss464spx_rearsight then
		table.list_append(self.parts.wpn_fps_snp_moss464spx_rearsight.forbids, {"wpn_fps_snp_moss464spx_sight_rail","wpn_fps_upg_o_45iron"})
	end
	local custom_wpn_id = "wpn_fps_snp_moss464spx"
	local stance_wpn_id = "wpn_fps_snp_tti"
	local offset = Vector3(-2.42, 5, -2.1)
	for _, part_id in pairs(self[custom_wpn_id].uses_parts) do
		if self.parts[part_id] then
			if self.parts[part_id].type == "sight" or self.parts[part_id].a_obj == "a_o" then
				if self.parts[part_id].stance_mod and self.parts[part_id].stance_mod[stance_wpn_id] then
					self.parts[part_id].stance_mod[custom_wpn_id] = deep_clone(self.parts[part_id].stance_mod[stance_wpn_id])
				end
			end
		end
	end
end)