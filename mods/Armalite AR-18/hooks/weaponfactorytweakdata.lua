Hooks:PostHook(WeaponFactoryTweakData, "create_bonuses", "AR18ModInit", function(self)
	if self.parts.wpn_fps_ass_ar18_irons and self.parts.wpn_fps_upg_ar18_irons_custom and self.parts.wpn_fps_upg_ar18_scope then
		table.list_append(self.parts.wpn_fps_ass_ar18_irons.forbids, {"wpn_fps_ass_ar18_sight_rail"})
		table.list_append(self.parts.wpn_fps_upg_ar18_irons_custom.forbids, {"wpn_fps_ass_ar18_sight_rail"})
		table.list_append(self.parts.wpn_fps_upg_ar18_scope.forbids, {"wpn_fps_ass_ar18_sight_rail"})
	end
	local custom_wpn_id = "wpn_fps_ass_ar18"
	local offset = Vector3(0.4, -4, 0.075)
	for _, part_id in pairs(self.parts) do
		if part_id.type == "second_sight" and part_id.a_obj == "a_o" and part_id.stance_mod[custom_wpn_id] then
			if part_id.stance_mod[custom_wpn_id].translation and not part_id.stance_mod[custom_wpn_id].offset_applied then
				part_id.stance_mod[custom_wpn_id].translation = (part_id.stance_mod[custom_wpn_id].translation + offset)
				part_id.stance_mod[custom_wpn_id].offset_applied = true
			end
		end
	end
end)
Hooks:PostHook(WeaponFactoryTweakData, "create_charms", "AR18ModInit", function(self)
	for _, charm_id in pairs(self.wpn_fps_ass_ar18.uses_parts) do
		if self.parts[charm_id].type == "charm" then
			self.parts.wpn_fps_upg_ar18_stock_folded.override[charm_id] = {parent="stock"}
		end
	end
end)