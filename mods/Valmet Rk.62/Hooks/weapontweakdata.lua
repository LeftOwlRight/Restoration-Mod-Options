Hooks:PostHook(WeaponTweakData, "init", "Rk62ModInit", function(self)
if not self.rk62_crew then
	self.rk62_crew = deep_clone(self.akm_crew)
end
if self.SetupAttachmentPoint then
	self:SetupAttachmentPoint("rk62", {
		name = "a_b",
		base_a_obj = "a_b",
		position = Vector3(0, -5.825, -0.7)
	})
	self:SetupAttachmentPoint("rk62", {
		name = "a_fl_railed",
		base_a_obj = "a_fl",
		position = Vector3(-0.1, -4, -0.6)
	})
	self:SetupAttachmentPoint("rk62", {
		name = "a_o",
		base_a_obj = "a_o_sm",
		position = Vector3(0, 0, -0.7)
	})
	self:SetupAttachmentPoint("rk62", {
		name = "a_s",
		base_a_obj = "a_s",
		position = Vector3(0, -1, 0)
	})
	self:SetupAttachmentPoint("rk62", {
		name = "a_s_vanilia",
		base_a_obj = "a_s",
		position = Vector3(0, 0, 0)
	})
	self:SetupAttachmentPoint("rk62", {
		name = "a_o_railed",
		base_a_obj = "a_o",
		position = Vector3(0, -17, 0.5)
	})
	self:SetupAttachmentPoint("rk62", {
		name = "a_o_railed_45",
		base_a_obj = "a_o",
		position = Vector3(0, -24, 0.5)
	})
end
end)