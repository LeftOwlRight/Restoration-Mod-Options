Hooks:PostHook(WeaponFactoryTweakData, "init", "Rk62ModInit", function(self)
	self.wpn_fps_ass_rk62.adds = {
		wpn_fps_upg_o_specter = {
			"wpn_fps_ass_rk62_rail"
		},
		wpn_fps_upg_o_aimpoint = {
			"wpn_fps_ass_rk62_rail"
		},
		wpn_fps_upg_o_aimpoint_2 = {
			"wpn_fps_ass_rk62_rail"
		},
		wpn_fps_upg_o_docter = {
			"wpn_fps_ass_rk62_rail"
		},
		wpn_fps_upg_o_eotech = {
			"wpn_fps_ass_rk62_rail"
		},
		wpn_fps_upg_o_t1micro = {
			"wpn_fps_ass_rk62_rail"
		},
		wpn_fps_upg_o_cmore = {
			"wpn_fps_ass_rk62_rail"
		},
		wpn_fps_upg_o_acog = {
			"wpn_fps_ass_rk62_rail"
		},
		wpn_fps_upg_o_cs = {
			"wpn_fps_ass_rk62_rail"
		},
		wpn_fps_upg_o_eotech_xps = {
			"wpn_fps_ass_rk62_rail"
		},
		wpn_fps_upg_o_reflex = {
			"wpn_fps_ass_rk62_rail"
		},
		wpn_fps_upg_o_rx01 = {
			"wpn_fps_ass_rk62_rail"
		},
		wpn_fps_upg_o_rx30 = {
			"wpn_fps_ass_rk62_rail"
		},
		wpn_fps_upg_o_spot = {
			"wpn_fps_ass_rk62_rail"
		}
	}
local custom_weapon_id = "wpn_fps_ass_rk62"
local stance_base_id = "wpn_fps_ass_akm"
local uses_sights = {"wpn_fps_upg_o_specter","wpn_fps_upg_o_aimpoint","wpn_fps_upg_o_docter","wpn_fps_upg_o_eotech","wpn_fps_upg_o_t1micro","wpn_fps_upg_o_cmore","wpn_fps_upg_o_aimpoint_2","wpn_fps_upg_o_acog","wpn_fps_upg_o_eotech_xps","wpn_fps_upg_o_reflex","wpn_fps_upg_o_rx01","wpn_fps_upg_o_rx30","wpn_fps_upg_o_cs","wpn_fps_upg_o_45rds","wpn_fps_upg_o_spot","wpn_fps_upg_o_45rds_v2","wpn_fps_upg_o_xpsg33_magnifier"}
--
for _, sight_id in pairs(uses_sights) do
	if self.parts[sight_id].stance_mod[stance_base_id] then
		self.parts[sight_id].stance_mod[custom_weapon_id] = deep_clone(self.parts[sight_id].stance_mod[stance_base_id])
	else
		log("[ERROR] " .. custom_weapon_id .. " Missing stance_mod data for: " .. sight_id, stance_base_id)
	end
end
	self.parts.wpn_fps_upg_o_45rds.stance_mod.wpn_fps_ass_rk62 = {translation = Vector3(-2.8, 2, -13.83),rotation = Rotation(0, 0, -45)}
	self.parts.wpn_fps_upg_o_45rds_v2.stance_mod.wpn_fps_ass_rk62 = deep_clone(self.parts.wpn_fps_upg_o_45rds.stance_mod.wpn_fps_ass_rk62)
end)