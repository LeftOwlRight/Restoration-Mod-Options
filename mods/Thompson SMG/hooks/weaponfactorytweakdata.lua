Hooks:PostHook( WeaponFactoryTweakData, "init", "ThompsonSMGModInit", function(self)
	self.wpn_fps_smg_tommy.adds = {
		wpn_fps_upg_o_specter = {
			"wpn_fps_smg_thompson_o_adapter"
		},
		wpn_fps_upg_o_aimpoint = {
			"wpn_fps_smg_thompson_o_adapter"
		},
		wpn_fps_upg_o_aimpoint_2 = {
			"wpn_fps_smg_thompson_o_adapter"
		},
		wpn_fps_upg_o_docter = {
			"wpn_fps_smg_thompson_o_adapter"
		},
		wpn_fps_upg_o_eotech = {
			"wpn_fps_smg_thompson_o_adapter"
		},
		wpn_fps_upg_o_t1micro = {
			"wpn_fps_smg_thompson_o_adapter"
		},
		wpn_fps_upg_o_cmore = {
			"wpn_fps_smg_thompson_o_adapter"
		},
		wpn_fps_upg_o_acog = {
			"wpn_fps_smg_thompson_o_adapter"
		},
		wpn_fps_upg_o_cs = {
			"wpn_fps_smg_thompson_o_adapter"
		},
		wpn_fps_upg_o_eotech_xps = {
			"wpn_fps_smg_thompson_o_adapter"
		},
		wpn_fps_upg_o_reflex = {
			"wpn_fps_smg_thompson_o_adapter"
		},
		wpn_fps_upg_o_rx01 = {
			"wpn_fps_smg_thompson_o_adapter"
		},
		wpn_fps_upg_o_rx30 = {
			"wpn_fps_smg_thompson_o_adapter"
		},
		wpn_fps_upg_fl_ass_smg_sho_surefire = {
			"wpn_fps_smg_thompson_fl_adapter"
		},
		wpn_fps_upg_fl_ass_smg_sho_peqbox = {
			"wpn_fps_smg_thompson_fl_adapter"
		},
		wpn_fps_upg_fl_ass_peq15 = {
			"wpn_fps_smg_thompson_fl_adapter"
		},
		wpn_fps_upg_fl_ass_laser = {
			"wpn_fps_smg_thompson_fl_adapter"
		},
		wpn_fps_upg_fl_dbal_laser = {
			"wpn_fps_smg_thompson_fl_adapter"
		},
		wpn_fps_upg_fl_ass_utg = {
			"wpn_fps_smg_thompson_fl_adapter"
		},
		wpn_fps_upg_o_spot = {
			"wpn_fps_smg_thompson_o_adapter"
		},
		wpn_fps_upg_o_bmg = {
			"wpn_fps_smg_thompson_o_adapter"
		},
		wpn_fps_upg_o_uh = {
			"wpn_fps_smg_thompson_o_adapter"
		},
		wpn_fps_upg_o_fc1 = {
			"wpn_fps_smg_thompson_o_adapter"
		},
		wpn_fps_upg_o_tf90 = {
			"wpn_fps_smg_thompson_o_adapter"
		},
		wpn_fps_upg_o_poe = {
			"wpn_fps_smg_thompson_o_adapter"
		},
		wpn_fps_upg_o_hamr = {
			"wpn_fps_smg_thompson_o_adapter"
		},
		wpn_fps_upg_o_atibal = {
			"wpn_fps_smg_thompson_o_adapter"
		},
		wpn_fps_upg_o_health = {
			"wpn_fps_smg_thompson_o_adapter"
		}
	}
	self.wpn_fps_smg_tommy.override = {
		wpn_fps_upg_ns_pis_putnik = {
			a_obj = "a_ns",
			parent = "barrel"
		}
	}
end )