local math_cos = math.cos
local math_sin = math.sin
local math_acos = math.acos

local mrot_set_zero = mrotation.set_zero
local mrot_multiply = mrotation.multiply
local mrot_invert = mrotation.invert
local mrot_slerp = mrotation.slerp
local mrot_rotation_difference = mrotation.rotation_difference
local function mrot_set(rotation, set)
	mrot_set_zero(rotation)
	mrot_multiply(rotation, set)
end

Hooks:PostHook(FPCameraPlayerBase, "init", "eyeAimFPCameraPlayerBaseInit", function(self, unit)
	self._eyeaim_object = self._unit:get_object(Idstring("eyeAim"))
end)

local function get_rotation_angle(rot)
	local yaw = rot:yaw()
	local pitch = rot:pitch()
	local roll = rot:roll()

	local cos_yaw = math_cos(yaw/2)
	local sin_yaw = math_sin(yaw/2)

	local cos_pitch = math_cos(pitch/2)
	local sin_pitch = math_sin(pitch/2)

	local cos_roll = math_cos(roll/2)
	local sin_roll = math_sin(roll/2)

	return 2 * math_acos((cos_yaw*cos_pitch*cos_roll) - (sin_yaw*sin_pitch*sin_roll))
end

local temp_delta_rot = Rotation()
local function rotate_towards(rotation, from, to, max_degrees_delta)
	mrot_rotation_difference(temp_delta_rot, from, to)
	local angle = get_rotation_angle(temp_delta_rot)

	if angle == 0 then
		mrot_set(rotation, to)
		return
	end

	mrot_slerp(rotation, from, to, math.min(1, max_degrees_delta / angle))

	return 
end

local angle_per_second = 30.0
local use_blending = false

local inverted_shoulder_rotation = Rotation()
local rotation_difference = Rotation()
local current_eyeaim_rotation = Rotation()
local target_eyeaim_rotation = Rotation()
Hooks:PostHook(FPCameraPlayerBase, "update", "eyeAimFPCameraPlayerBaseUpdate", function(self, unit, t, dt)
	mrot_set(inverted_shoulder_rotation, self._shoulder_stance.rotation)
	mrot_invert(inverted_shoulder_rotation)

	local rotation_difference = self._unit:orientation_object():to_local(self._eyeaim_object:rotation())

	mrot_set(target_eyeaim_rotation, self._shoulder_stance.rotation)
	mrot_multiply(target_eyeaim_rotation, rotation_difference)
	mrot_multiply(target_eyeaim_rotation, inverted_shoulder_rotation)

	if use_blending then
		rotate_towards(current_eyeaim_rotation, current_eyeaim_rotation, target_eyeaim_rotation, angle_per_second * dt)
	else
		mrot_set(current_eyeaim_rotation, target_eyeaim_rotation)
	end

	self._parent_unit:camera():set_eyeaim_rotation(current_eyeaim_rotation)
end)