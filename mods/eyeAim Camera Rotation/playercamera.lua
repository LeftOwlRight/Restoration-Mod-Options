local mrot_set_yaw_pitch_roll = mrotation.set_yaw_pitch_roll
local mrot_multiply = mrotation.multiply
local mrot_y = mrotation.y
local mrot_z = mrotation.z

local mvec_multiply = mvector3.multiply
local mvec_add = mvector3.add

local math_floor = math.floor
local math_clamp = math.clamp
local math_abs = math.abs

Hooks:PostHook(PlayerCamera, "init", "eyeAimPlayerCameraInit", function(self, unit)
	self._eyeaim_rotation = Rotation()
end)

function PlayerCamera:set_eyeaim_rotation(eyeaim_rot)
	mrot_set_yaw_pitch_roll(self._eyeaim_rotation, eyeaim_rot:yaw(), eyeaim_rot:pitch(), eyeaim_rot:roll())
end

local mvec1 = Vector3()
local eyeaim_rot = Rotation()
function PlayerCamera:set_rotation(rot)
	if _G.IS_VR then
		self._camera_object:set_rotation(rot)
	else
		mrot_set_yaw_pitch_roll(eyeaim_rot, rot:yaw(), rot:pitch(), rot:roll())
		mrot_multiply(eyeaim_rot, self._eyeaim_rotation)

		mrot_y(eyeaim_rot, mvec1)
		mvec_multiply(mvec1, 100000)
		mvec_add(mvec1, self._m_cam_pos)

		self._camera_controller:set_target(mvec1)

		mrot_z(eyeaim_rot, mvec1)

		self._camera_controller:set_default_up(mvec1)
	end

	mrot_set_yaw_pitch_roll(self._m_cam_rot, rot:yaw(), rot:pitch(), rot:roll())
	mrot_y(self._m_cam_rot, self._m_cam_fwd)

	local t = TimerManager:game():time()
	local sync_dt = t - self._last_sync_t
	local sync_yaw = rot:yaw()
	sync_yaw = sync_yaw % 360

	if sync_yaw < 0 then
		sync_yaw = 360 - sync_yaw
	end

	sync_yaw = math_floor(255 * sync_yaw / 360)
	local sync_pitch = nil

	if _G.IS_VR then
		sync_pitch = math_clamp(rot:pitch(), -30, 60) + 85
	else
		sync_pitch = math_clamp(rot:pitch(), -85, 85) + 85
	end

	sync_pitch = math_floor(127 * sync_pitch / 170)
	local angle_delta = math_abs(self._sync_dir.yaw - sync_yaw) + math_abs(self._sync_dir.pitch - sync_pitch)

	if tweak_data.network then
		local update_network = tweak_data.network.camera.network_sync_delta_t < sync_dt and angle_delta > 0 or tweak_data.network.camera.network_angle_delta < angle_delta
		local locked_look_dir = self._locked_look_dir_t and t < self._locked_look_dir_t

		if update_network then
			if _G.IS_VR then
				if locked_look_dir then
					if self._unit:hand():arm_simulation_enabled() then
						self._unit:hand():send_filtered("set_look_dir", sync_yaw, sync_pitch)
						self._unit:hand():send_inv_filtered("set_look_dir", self._locked_yaw, self._locked_pitch)
					else
						self._unit:network():send("set_look_dir", self._locked_yaw, self._locked_pitch)
					end
				else
					self._unit:network():send("set_look_dir", sync_yaw, sync_pitch)
				end
			else
				self._unit:network():send("set_look_dir", sync_yaw, sync_pitch)
			end

			self._sync_dir.yaw = sync_yaw
			self._sync_dir.pitch = sync_pitch
			self._last_sync_t = t
		end
	end
end