Hooks:PostHook(WeaponFactoryTweakData, "init", "TroglodyteModInit", function(self)

	self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_snp_troglodyte = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_snp_msr)
	self.parts.wpn_fps_upg_o_aimpoint.stance_mod.wpn_fps_snp_troglodyte = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_snp_msr)
	self.parts.wpn_fps_upg_o_docter.stance_mod.wpn_fps_snp_troglodyte = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_snp_msr)
	self.parts.wpn_fps_upg_o_eotech.stance_mod.wpn_fps_snp_troglodyte = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_snp_msr)
	self.parts.wpn_fps_upg_o_t1micro.stance_mod.wpn_fps_snp_troglodyte = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_snp_msr)
	self.parts.wpn_fps_upg_o_aimpoint_2.stance_mod.wpn_fps_snp_troglodyte = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_snp_msr)
	self.parts.wpn_fps_upg_o_eotech_xps.stance_mod.wpn_fps_snp_troglodyte = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_snp_msr)
	self.parts.wpn_fps_upg_o_reflex.stance_mod.wpn_fps_snp_troglodyte = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_snp_msr)
	self.parts.wpn_fps_upg_o_rx01.stance_mod.wpn_fps_snp_troglodyte = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_snp_msr)
	self.parts.wpn_fps_upg_o_rx30.stance_mod.wpn_fps_snp_troglodyte = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_snp_msr)
	self.parts.wpn_fps_upg_o_cmore.stance_mod.wpn_fps_snp_troglodyte = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_snp_msr)
	self.parts.wpn_fps_upg_o_cs.stance_mod.wpn_fps_snp_troglodyte = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_snp_msr)
	self.parts.wpn_fps_upg_o_acog.stance_mod.wpn_fps_snp_troglodyte = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_snp_msr)
	self.parts.wpn_fps_upg_o_spot.stance_mod.wpn_fps_snp_troglodyte = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_snp_msr)
	self.parts.wpn_fps_upg_o_shortdot.stance_mod.wpn_fps_snp_troglodyte = deep_clone(self.parts.wpn_fps_upg_o_shortdot.stance_mod.wpn_fps_snp_msr)
	self.parts.wpn_fps_upg_o_leupold.stance_mod.wpn_fps_snp_troglodyte = deep_clone(self.parts.wpn_fps_upg_o_leupold.stance_mod.wpn_fps_snp_msr)
	self.parts.wpn_fps_upg_o_box.stance_mod.wpn_fps_snp_troglodyte = deep_clone(self.parts.wpn_fps_upg_o_box.stance_mod.wpn_fps_snp_msr)

	self.parts.wpn_fps_upg_o_45rds.stance_mod.wpn_fps_snp_troglodyte = {translation = Vector3(-3.1, -5, -12.5),rotation = Rotation(0, 0, -45)}
	self.parts.wpn_fps_upg_o_45rds_v2.stance_mod.wpn_fps_snp_troglodyte = deep_clone(self.parts.wpn_fps_upg_o_45rds.stance_mod.wpn_fps_snp_troglodyte)
	self.parts.wpn_fps_upg_o_45iron.stance_mod.wpn_fps_snp_troglodyte = deep_clone(self.parts.wpn_fps_upg_o_45rds.stance_mod.wpn_fps_snp_troglodyte)
end)
