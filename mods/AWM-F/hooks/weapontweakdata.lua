Hooks:PostHook(WeaponTweakData, "init", "TroglodyteModInit", function(self)
if not self.troglodyte_crew then
	self.troglodyte_crew = deep_clone(self.msr_crew)
end
if self.SetupAttachmentPoint then
	self:SetupAttachmentPoint("troglodyte",{
		name = "a_b",
		base_a_obj = "a_b",
		position = Vector3(0, 15.4, 0.25),
		rotation = RotationCAP(0, 0, 0)
	})
	self:SetupAttachmentPoint("troglodyte",{
		name = "a_b_long",
		base_a_obj = "a_b",
		position = Vector3(0, 18.5, 0.25),
		rotation = RotationCAP(0, 0, 0)
	})
	self:SetupAttachmentPoint("troglodyte",{
		name = "a_o",
		base_a_obj = "a_o",
		position = Vector3(0, 0, -1.25),
		rotation = RotationCAP(0, 0, 0)
	})
	self:SetupAttachmentPoint("troglodyte",{
		name = "a_o_troglodyte",
		base_a_obj = "a_o",
		position = Vector3(0, 0, 0.2),
		rotation = RotationCAP(0, 0, 0)
	})
	self:SetupAttachmentPoint("troglodyte",{
		name = "a_b",
		base_a_obj = "a_b",
		position = Vector3(0, 0, 0),
		rotation = RotationCAP(0, 0, 0)
	})
	self:SetupAttachmentPoint("troglodyte",{
		name = "a_fl",
		base_a_obj = "a_fl",
		position = Vector3(-7.5, 15, 2.5),
		rotation = RotationCAP(0, 0, 180)
	})
	self:SetupAttachmentPoint("troglodyte",{
		name = "a_fl_troglodyte",
		base_a_obj = "a_fl",
		position = Vector3(-3.3, 48, -4.1),
		rotation = RotationCAP(0, 0, 90)
	})
end
end)