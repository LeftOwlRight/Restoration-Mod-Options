Hooks:PostHook(WeaponFactoryTweakData, "init", "omnisightinit", function(self)

self.parts.wpn_fps_upg_o_tf90.stance_mod.wpn_fps_shot_omni = {translation = Vector3(-0.066, 0, 0) + Vector3(0, 0, 0)}
self.parts.wpn_fps_upg_o_aimpoint.stance_mod.wpn_fps_shot_omni = {translation = Vector3(-0.066, 0, 0) + Vector3(0, 0, 0)}
self.parts.wpn_fps_upg_o_aimpoint_2.stance_mod.wpn_fps_shot_omni = {translation = Vector3(-0.066, 0, 0) + Vector3(0, 0, 0)}
self.parts.wpn_fps_upg_o_docter.stance_mod.wpn_fps_shot_omni = {translation = Vector3(-0.066, 0, 0) + Vector3(0, 0, 0)}
self.parts.wpn_fps_upg_o_eotech.stance_mod.wpn_fps_shot_omni = {translation = Vector3(-0.066, 0, 0) + Vector3(0, 0, 0)}
self.parts.wpn_fps_upg_o_t1micro.stance_mod.wpn_fps_shot_omni = {translation = Vector3(-0.066, 0, 0) + Vector3(0, 0, 0)}
self.parts.wpn_fps_upg_o_acog.stance_mod.wpn_fps_shot_omni = {translation = Vector3(-0.066, 0, 0) + Vector3(0, 0, 0)}
self.parts.wpn_fps_upg_o_cs.stance_mod.wpn_fps_shot_omni = {translation = Vector3(-0.066, 0, 0) + Vector3(0, 0, 0)}
self.parts.wpn_fps_upg_o_eotech_xps.stance_mod.wpn_fps_shot_omni = {translation = Vector3(-0.066, 0, 0) + Vector3(0, 0, 0)}
self.parts.wpn_fps_upg_o_reflex.stance_mod.wpn_fps_shot_omni = {translation = Vector3(-0.066, 0, 0) + Vector3(0, 0, 0)}
self.parts.wpn_fps_upg_o_rx01.stance_mod.wpn_fps_shot_omni = {translation = Vector3(-0.066, 0, 0) + Vector3(0, 0, 0)}
self.parts.wpn_fps_upg_o_rx30.stance_mod.wpn_fps_shot_omni = {translation = Vector3(-0.066, 0, 0) + Vector3(0, 0, 0)}
self.parts.wpn_fps_upg_o_cmore.stance_mod.wpn_fps_shot_omni = {translation = Vector3(-0.066, 0, 0) + Vector3(0, 0, 0)}
self.parts.wpn_fps_upg_o_spot.stance_mod.wpn_fps_shot_omni = {translation = Vector3(-0.066, 0, 0) + Vector3(0, 0, 0)}
self.parts.wpn_fps_upg_o_uh.stance_mod.wpn_fps_shot_omni = {translation = Vector3(-0.066, 0, 0) + Vector3(0, 0, 0)}
self.parts.wpn_fps_upg_o_fc1.stance_mod.wpn_fps_shot_omni = {translation = Vector3(-0.066, 0, 0) + Vector3(0, 0, 0)}
self.parts.wpn_fps_upg_o_bmg.stance_mod.wpn_fps_shot_omni = {translation = Vector3(-0.066, 0, 0) + Vector3(0, 0, 0)}
self.parts.wpn_fps_upg_o_xpsg33_magnifier.stance_mod.wpn_fps_shot_omni = {translation = Vector3(-0.066, 0, 0) + Vector3(0, 0, 0)}
self.parts.wpn_fps_upg_o_sig.stance_mod.wpn_fps_shot_omni = {translation = Vector3(-0.066, 0, 0) + Vector3(0, 0, 0)}
self.parts.wpn_fps_upg_o_poe.stance_mod.wpn_fps_shot_omni = {translation = Vector3(-0.066, 0, 0) + Vector3(0, 0, 0)}
self.parts.wpn_fps_upg_o_health.stance_mod.wpn_fps_shot_omni = {translation = Vector3(-0.066, 0, 0) + Vector3(0, 0, 0)}
self.parts.wpn_fps_upg_o_atibal.stance_mod.wpn_fps_shot_omni = {translation = Vector3(-0.066, 0, 0) + Vector3(0, 0, 0)}
self.parts.wpn_fps_upg_o_hamr.stance_mod.wpn_fps_shot_omni = {translation = Vector3(-0.066, 0, 0) + Vector3(0, 0, 0)}

	-- vmp support
	if BeardLib.Utils:FindMod("Vanilla Styled Weapon Mods") then
		table.insert(self.wpn_fps_shot_omni.uses_parts, "wpn_fps_ass_m4_g_sg")
		table.insert(self.wpn_fps_shot_omni.uses_parts, "wpn_fps_ass_m4_s_russian")

	self.parts.wpn_fps_upg_o_cqb.stance_mod.wpn_fps_shot_omni = 
	{
		translation = Vector3(0, 0, -0.2),
		rotation = Rotation(0, 0, 0)
	}

	else
		-- vanilla mod pack not installed
	end

	if BeardLib.Utils:FindMod("Vanilla Styled Weapon Mods Legacy") then
		table.insert(self.wpn_fps_shot_omni.uses_parts, "wpn_fps_m4_g_wrap")
	
	else
		-- vanilla mod pack legacy not installed
	end

	if BeardLib.Utils:FindMod("AR AK Mods") then
		table.insert(self.wpn_fps_shot_omni.uses_parts, "wpn_fps_upg_m4_fg_shepheard_long")
	
	else
		-- vanilla mod pack legacy not installed
	end

	self.parts.wpn_fps_upg_o_hamr_reddot.stance_mod.wpn_fps_shot_omni = 
	{
		translation = Vector3(0, 0, -3.65),
		rotation = Rotation(0, 0, 0)
	}
	self.parts.wpn_fps_upg_o_atibal_reddot.stance_mod.wpn_fps_shot_omni = 
	{
		translation = Vector3(0, 0, -6.15),
		rotation = Rotation(0, 0, 0)
	}
	self.parts.wpn_fps_upg_o_specter_piggyback.stance_mod.wpn_fps_shot_omni = 
	{
		translation = Vector3(0.05, 0, -3.35),
		rotation = Rotation(0, 0, 0)
	}
	self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_shot_omni = {
		translation = Vector3(-0.04, -0.8, -0.135),
		rotation = Rotation(-0.11, 0 ,-0.7)
	}

end )