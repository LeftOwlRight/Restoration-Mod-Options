Hooks:PostHook( WeaponFactoryTweakData, "init", "hpbInit", function(self)
self.parts.wpn_fps_pis_hpb_b_standard.stance_mod = {
			wpn_fps_pis_hpb = { 
				translation = Vector3(-0.005, 2, -0.4),
				rotation = Rotation(0.05, 0.6, -1.0422e-010)
			}
		}
self.wpn_fps_pis_hpb.animations = {
	magazine_empty="last_recoil",
	reload = "reload",
	reload_not_empty = "reload_not_empty",
	fire = "recoil",
	fire_steelsight = "recoil"
}
self.parts.wpn_fps_pis_hpb_a_c45.custom_stats = {
	ammo_pickup_max_mul = 1.75
}
self.wpn_fps_pis_hpb.override = {
	wpn_fps_upg_o_rmr = {parent = "slide", unit="units/mods/weapons/wpn_fps_pis_hpb_pts/wpn_fps_pis_hpb_o_standard"},
	wpn_fps_upg_pis_ns_flash = {parent = "barrel", a_obj = "a_ns"},
	wpn_fps_upg_ns_pis_medium_slim = {parent = "barrel", a_obj = "a_ns"},
	wpn_fps_upg_ns_ass_filter = {parent = "barrel", a_obj = "a_ns"},
	wpn_fps_upg_ns_pis_jungle = {parent = "barrel", a_obj = "a_ns"},
	wpn_fps_upg_ns_pis_large = {parent = "barrel", a_obj = "a_ns"},
	wpn_fps_upg_ns_pis_medium = {parent = "barrel", a_obj = "a_ns"},
	wpn_fps_upg_ns_pis_small = {parent = "barrel", a_obj = "a_ns"},
	wpn_fps_upg_ns_pis_large_kac = {parent = "barrel", a_obj = "a_ns"},
	wpn_fps_upg_ns_pis_medium_gem = {parent = "barrel", a_obj = "a_ns"},
	wpn_fps_upg_ns_pis_ipsccomp = {parent = "barrel", a_obj = "a_ns"},
	wpn_fps_pis_hpb_comp = {parent = "barrel", a_obj = "a_ns"},
	wpn_fps_pis_hpb_comp2 = {parent = "barrel", a_obj = "a_ns"},
	wpn_fps_upg_ns_pis_meatgrinder = {parent = "barrel", a_obj = "a_ns"}
}
self.parts.wpn_fps_pis_hpb_body_pearl.override = {
		wpn_fps_pis_hpb_sl_standard = {
			unit = "units/mods/weapons/wpn_fps_pis_hpb_pts/wpn_fps_pis_hpb_sl_pearl"
		}
}
self.parts.wpn_fps_pis_hpb_body_blued.override = {
		wpn_fps_pis_hpb_sl_standard = {
			unit = "units/mods/weapons/wpn_fps_pis_hpb_pts/wpn_fps_pis_hpb_sl_blued"
		}
}
self.parts.wpn_fps_pis_hpb_body_gold.override = {
		wpn_fps_pis_hpb_sl_standard = {
			unit = "units/mods/weapons/wpn_fps_pis_hpb_pts/wpn_fps_pis_hpb_sl_gold"
		}
}
self.parts.wpn_fps_upg_o_rmr.stance_mod.wpn_fps_pis_hpb =  {translation = Vector3(-0.05, 12, -0.66),rotation = Rotation(0, 0, 0)}

if SystemFS:exists("assets/mod_overrides/Sneaky Suppressor Pack/main.xml") then
table.insert(self.wpn_fps_pis_hpb.uses_parts, "wpn_fps_ass_ns_g_sup2")
table.insert(self.wpn_fps_pis_hpb.uses_parts, "wpn_fps_ass_ns_g_sup1")
self.wpn_fps_pis_hpb.override.wpn_fps_ass_ns_g_sup1 = {
	parent = "barrel", a_obj = "a_ns"
}
self.wpn_fps_pis_hpb.override.wpn_fps_ass_ns_g_sup2 = {
	parent = "barrel", a_obj = "a_ns"
}
end

end )