Hooks:PostHook(WeaponFactoryTweakData, "init", "m4_usasoc_factorytweakdata_init", function(self)
	
	self.parts.wpn_fps_ass_m4_usasoc_magazine.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}
	self.parts.wpn_fps_ass_m4_usasoc_magazine_20.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}
	self.parts.wpn_fps_ass_m4_usasoc_magazine_dd.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}
	self.parts.wpn_fps_ass_m4_usasoc_magazine_mike4.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}
	self.parts.wpn_fps_ass_m4_usasoc_magazine_pmag.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}
	self.parts.wpn_fps_ass_m4_usasoc_magazine_xmag.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}

	self.parts.wpn_fps_ass_m4_usasoc_angled_sight_rmr.stance_mod.wpn_fps_ass_m4_usasoc = {
            translation = Vector3(-1.3, -7, -9),
             rotation = Rotation(0.11, 0.1, -45)
            }

	if BeardLib.Utils:FindMod("Tacticool Handguard Pack") then
		table.insert(self.wpn_fps_ass_m4_usasoc.uses_parts, "wpn_fps_upg_m4_fg_urx10")	
	else
		-- Frenchy's Tactical Handguards isnt installed
	end			
	
end)