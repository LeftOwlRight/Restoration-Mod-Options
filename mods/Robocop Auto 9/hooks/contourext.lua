ContourExt._types.ocpo_target = {
	priority = 5,
	color = Vector3(146/255,222/256,161/255),
	material_swap_required = true,
	fadeout = 1
}
ContourExt.indexed_types[#ContourExt.indexed_types+1] = "ocpo_target"