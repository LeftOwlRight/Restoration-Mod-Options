Hooks:PostHook(WeaponFactoryTweakData, "init", "akilo_2022_weaponfactorytweakdata_init", function(self)

-- Mag stuff
 self.parts.wpn_fps_ass_akilo_2022_magazine.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}
  self.parts.wpn_fps_ass_akilo_2022_smag.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}		
 self.parts.wpn_fps_ass_akilo_2022_xmag.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}
 self.parts.wpn_fps_ass_akilo_2022_magazine_t9damage.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}
self.parts.wpn_fps_ass_akilo_2022_magazine_akilo74.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}
 self.parts.wpn_fps_ass_akilo_2022_magazine_akilo105.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}
  self.parts.wpn_fps_ass_akilo_2022_smag_akilo105.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}

 self.parts.wpn_fps_ass_akilo_2022_xmag_akilo105.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}
 self.parts.wpn_fps_ass_akilo_2022_xmag_large_akilo105.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}	
 self.parts.wpn_fps_ass_akilo_2022_magazine_t9damage_545.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}
 self.parts.wpn_fps_ass_akilo_2022_magazine_mw2_545.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}
self.parts.wpn_fps_ass_akilo_2022_magazine_mw2.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}		

self.parts.wpn_fps_ass_akilo_2022_magazine_rkilo.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}		
				
self.parts.wpn_fps_ass_akilo_2022_xmag_rkilo.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}		
self.parts.wpn_fps_ass_akilo_2022_magazine_rkilo_545.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}		
self.parts.wpn_fps_ass_akilo_2022_xmag_rkilo_545.animations = {
		reload_not_empty = "reload_not_empty",
		reload = "reload"
		}		

-- thanks Mira
	self.parts.wpn_fps_ass_akilo_2022_receiver_akilo74.override = {
		wpn_fps_ass_akilo_2022_irons = {
			stance_mod = {
				wpn_fps_ass_akilo105_2022 = {
					translation = Vector3(0, -25, 0.8),
					rotation = Rotation(0, 0, 0)
				}
			}	
		},
	}
	
end)