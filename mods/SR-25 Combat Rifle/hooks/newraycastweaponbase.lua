local old_weapon_tweak_data = NewRaycastWeaponBase.weapon_tweak_data
function NewRaycastWeaponBase:weapon_tweak_data(...)
	local weapon_tweak_data = old_weapon_tweak_data(self, ...)

	if not self._parts or self._name_id ~= "iuhTTIPlus" then
		return weapon_tweak_data 
	end
	
	if self._parts.wpn_fps_snp_iuhTTIPlus_vg then weapon_tweak_data.use_stance = "iuhTTIPlus" end 
	if self._parts.wpn_fps_snp_iuhTTIPlus_vg then weapon_tweak_data.weapon_hold = "iuhTTIPlusVg" end
	if self._parts.wpn_fps_snp_iuhTTIPlus_vg then weapon_tweak_data.animations.reload_name_id = "iuhTTIPlusVg" end

	if self._parts.wpn_fps_snp_iuhTTIPlus_gl_m203 then weapon_tweak_data.use_stance = "iuhTTIPlus" end 
	if self._parts.wpn_fps_snp_iuhTTIPlus_gl_m203 then weapon_tweak_data.weapon_hold = "iuhTTIPlus" end
	-- if self._parts.wpn_fps_snp_iuhTTIPlus_gl_m203 then weapon_tweak_data.animations.reload_name_id = "iuhTTIPlusM203" end

	return weapon_tweak_data
end

