Hooks:PostHook( WeaponFactoryTweakData, "init", "fort500Init", function(self)
	local custom_wpn_id = "wpn_fps_shot_f500"
	local stance_wpn_id = "wpn_fps_shot_serbu"
	for _, part_id in pairs(self[custom_wpn_id].uses_parts) do
		if self.parts[part_id].type == "sight" or self.parts[part_id].type == "gadget" then
			if self.parts[part_id].stance_mod and self.parts[part_id].stance_mod[stance_wpn_id] then
				self.parts[part_id].stance_mod[custom_wpn_id] = deep_clone(self.parts[part_id].stance_mod[stance_wpn_id])
			end
		end
	end
	local switch_id = {
		"wpn_fps_upg_o_specter_piggyback",
		"wpn_fps_upg_o_cs_piggyback",
		"wpn_fps_upg_o_hamr_reddot",
		"wpn_fps_upg_o_atibal_reddot"
	}
	for index, part_id in pairs(switch_id) do
		if self.parts[part_id].stance_mod[stance_wpn_id] then
			self.parts[part_id].stance_mod[custom_wpn_id] = deep_clone(self.parts[part_id].stance_mod[stance_wpn_id])
		end
	end
end )