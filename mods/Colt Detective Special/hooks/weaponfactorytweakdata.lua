Hooks:PostHook( WeaponFactoryTweakData, "init", "ColtDSModInit", function(self)

	
	self.wpn_fps_pis_coltds.adds = {
		wpn_fps_upg_o_specter = {
			"wpn_fps_pis_coltds_o_rail"
		},
		wpn_fps_upg_o_aimpoint = {
			"wpn_fps_pis_coltds_o_rail"
		},
		wpn_fps_upg_o_aimpoint_2 = {
			"wpn_fps_pis_coltds_o_rail"
		},
		wpn_fps_upg_o_docter = {
			"wpn_fps_pis_coltds_o_rail"
		},
		wpn_fps_upg_o_eotech = {
			"wpn_fps_pis_coltds_o_rail"
		},
		wpn_fps_upg_o_t1micro = {
			"wpn_fps_pis_coltds_o_rail"
		},
		wpn_fps_upg_o_cmore = {
			"wpn_fps_pis_coltds_o_rail"
		},
		wpn_fps_upg_o_cs = {
			"wpn_fps_pis_coltds_o_rail"
		},
		wpn_fps_upg_o_reflex = {
			"wpn_fps_pis_coltds_o_rail"
		},
		wpn_fps_upg_o_acog = {
			"wpn_fps_pis_coltds_o_rail"
		},
		wpn_fps_upg_o_eotech_xps = {
			"wpn_fps_pis_coltds_o_rail"
		},
		wpn_fps_upg_o_rx01 = {
			"wpn_fps_pis_coltds_o_rail"
		},
		wpn_fps_upg_o_rx30 = {
			"wpn_fps_pis_coltds_o_rail"
		},
		wpn_fps_upg_o_spot = {
			"wpn_fps_pis_coltds_o_rail"
		},
		wpn_fps_upg_o_fc1 = {
			"wpn_fps_pis_coltds_o_rail"
		},
		wpn_fps_upg_fl_pis_laser = {
			"wpn_fps_pis_coltds_fl_rail"
		},
		wpn_fps_upg_fl_pis_tlr1 = {
			"wpn_fps_pis_coltds_fl_rail"
		},
		wpn_fps_upg_fl_pis_perst = {
			"wpn_fps_pis_coltds_fl_rail"
		},
		wpn_fps_upg_fl_pis_crimson = {
			"wpn_fps_pis_coltds_fl_rail"
		},
		wpn_fps_upg_fl_pis_x400v = {
			"wpn_fps_pis_coltds_fl_rail"
		},
		wpn_fps_upg_fl_pis_m3x = {
			"wpn_fps_pis_coltds_fl_rail"
		}
	}
	self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_pis_coltds = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_pis_rage)
	self.parts.wpn_fps_upg_o_aimpoint.stance_mod.wpn_fps_pis_coltds = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_pis_rage)
	self.parts.wpn_fps_upg_o_docter.stance_mod.wpn_fps_pis_coltds = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_pis_rage)
	self.parts.wpn_fps_upg_o_eotech.stance_mod.wpn_fps_pis_coltds = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_pis_rage)
	self.parts.wpn_fps_upg_o_t1micro.stance_mod.wpn_fps_pis_coltds = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_pis_rage)
	self.parts.wpn_fps_upg_o_aimpoint_2.stance_mod.wpn_fps_pis_coltds = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_pis_rage)
	self.parts.wpn_fps_upg_o_eotech_xps.stance_mod.wpn_fps_pis_coltds = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_pis_rage)
	self.parts.wpn_fps_upg_o_reflex.stance_mod.wpn_fps_pis_coltds = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_pis_rage)
	self.parts.wpn_fps_upg_o_rx01.stance_mod.wpn_fps_pis_coltds = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_pis_rage)
	self.parts.wpn_fps_upg_o_rx30.stance_mod.wpn_fps_pis_coltds = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_pis_rage)
	self.parts.wpn_fps_upg_o_cmore.stance_mod.wpn_fps_pis_coltds = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_pis_rage)
	self.parts.wpn_fps_upg_o_cs.stance_mod.wpn_fps_pis_coltds = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_pis_rage)
	self.parts.wpn_fps_upg_o_acog.stance_mod.wpn_fps_pis_coltds = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_pis_rage)
	self.parts.wpn_fps_upg_o_spot.stance_mod.wpn_fps_pis_coltds = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_pis_rage)
	self.parts.wpn_fps_upg_o_fc1.stance_mod.wpn_fps_pis_coltds = deep_clone(self.parts.wpn_fps_upg_o_specter.stance_mod.wpn_fps_pis_rage)
end )