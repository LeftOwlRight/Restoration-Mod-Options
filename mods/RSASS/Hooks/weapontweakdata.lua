Hooks:PostHook(WeaponTweakData, "init", "RSASSwtdModInit", function(self)

    self:SetupAttachmentPoint("new_m4", {
        name = "a_s_m4_rsasshandguard",
        base_a_obj = "a_fg",
        position = Vector3( 0, -1, -0.23),
    })
    self:SetupAttachmentPoint("m16", {
        name = "a_s_m4_rsasshandguard",
        base_a_obj = "a_fg",
        position = Vector3( 0, -1, -0.23),
    })		
    self:SetupAttachmentPoint("rsass", {
        name = "a_fl",
        base_a_obj = "a_fl",
        position = Vector3( 0.35, 10, 0.33),
    })	
    self:SetupAttachmentPoint("rsass", {
        name = "a_b",
        base_a_obj = "a_b",
        position = Vector3(0, 3.8, 0.375),
    })
    self:SetupAttachmentPoint("rsass", {
        name = "a_b_short",
        base_a_obj = "a_b",
        position = Vector3(0, -10, 0.375),
    })	
    self:SetupAttachmentPoint("rsass", {
        name = "a_o",
        base_a_obj = "a_o",
        position = Vector3(0, 0, 0.25),
    })
    self:SetupAttachmentPoint("rsass", {
        name = "a_g",
        base_a_obj = "a_g",
        position = Vector3(0, 0.3, -0.155),
    })		
    self:SetupAttachmentPoint("rsass", {
        name = "a_g_rsass",
        base_a_obj = "a_g",
        position = Vector3(-0, 0, 0),
    })	
    self:SetupAttachmentPoint("rsass", {
        name = "a_bolt",
        base_a_obj = "a_body",
        position = Vector3(-0.7, 13.15, -1),
    })	
    self:SetupAttachmentPoint("rsass", {
        name = "a_g_grippy",
        base_a_obj = "a_g",
        position = Vector3(0, 0.315, 0),
    })		
    self:SetupAttachmentPoint("rsass", {
        name = "a_s_m4ubr",
        base_a_obj = "a_s",
        position = Vector3(0, 1.4, 0.2),
    })	
    self:SetupAttachmentPoint("rsass", {
        name = "a_s_m4ubr_col",
        base_a_obj = "a_s",
        position = Vector3(0, 2, 0.2),
    })	
    self:SetupAttachmentPoint("rsass", {
        name = "a_s_m4ubrmod",
        base_a_obj = "a_s",
        position = Vector3(0, -0.6, 0.3),
    })		
    self:SetupAttachmentPoint("rsass", {
        name = "a_s",
        base_a_obj = "a_s",
        position = Vector3(0, 0.9, 0.3),
    })	
    self:SetupAttachmentPoint("rsass", {
        name = "a_s_geronimo",
        base_a_obj = "a_s",
        position = Vector3(0, 4.79, 0.5),
    })	
    self:SetupAttachmentPoint("rsass", {
        name = "a_s_rsass",
        base_a_obj = "a_s",
        position = Vector3(0, 0, 0),
    })		
end )