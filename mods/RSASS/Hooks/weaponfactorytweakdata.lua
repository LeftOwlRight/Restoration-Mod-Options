Hooks:PostHook(WeaponFactoryTweakData, "init", "RSASSwftdModInit", function(self)
self.wpn_fps_ass_rsass.override.wpn_fps_upg_m4_s_ubr = {forbids = {"wpn_fps_rsass_stock_adapter_cst"}}
self.wpn_fps_ass_rsass.override.wpn_fps_rsass_stock = {override = {wpn_fps_rsass_stock_adapter_cst = { unit="units/mods/weapons/wpn_fps_ass_rsass_pts/wpn_fps_rsass_stock/wpn_fps_rsass_stock_adapter_a2"}}}
self.wpn_fps_ass_rsass.override.wpn_fps_upg_m4_s_ubr_col = {forbids = {"wpn_fps_rsass_stock_adapter_cst"}}
self.wpn_fps_ass_rsass.override.wpn_fps_upg_s_devgru = {forbids = {"wpn_fps_rsass_stock_adapter_cst"}}

if self.parts.wpn_fps_upg_o_burris_ff3 then
self.parts.wpn_fps_upg_o_burris_ff3.override = self.parts.wpn_fps_upg_o_burris_ff3.override or {}
self.parts.wpn_fps_upg_o_burris_ff3.override.wpn_fps_rsass_ironsight_front_folded = {unit="units/payday2/weapons/wpn_upg_dummy/wpn_upg_dummy"}
self.parts.wpn_fps_upg_o_burris_ff3.override.wpn_fps_rsass_ironsight_rear_folded = {unit="units/payday2/weapons/wpn_upg_dummy/wpn_upg_dummy"}
end

if self.parts.wpn_fps_rsass_barrel_short then
    self.wpn_fps_ass_rsass.override.wpn_fps_rsass_barrel_short = {a_obj="a_b_short"}
end	
if self.parts.wpn_fps_upg_fl_ass_laser then
    self.wpn_fps_ass_rsass.override.wpn_fps_upg_fl_ass_laser = {a_obj="a_fl_compl"}
end	
if self.parts.wpn_fps_upg_o_45rds_v2 then
    self.wpn_fps_ass_rsass.override.wpn_fps_upg_o_45rds_v2 = {a_obj="a_o_rktpnt45"}
end	
if self.parts.wpn_fps_rsass_grip then
    self.wpn_fps_ass_rsass.override.wpn_fps_rsass_grip = {a_obj="a_g_rsass"}
end
if self.parts.wpn_fps_upg_s_m4_ubr then
    self.wpn_fps_ass_rsass.override.wpn_fps_upg_s_m4_ubr = {a_obj="a_s_m4ubrmod"}
end
if self.parts.wpn_fps_snp_tti_g_grippy then
    self.wpn_fps_ass_rsass.override.wpn_fps_snp_tti_g_grippy = {a_obj="a_g_grippy"}
end
if self.parts.wpn_fps_upg_s_devgru then
    self.wpn_fps_ass_rsass.override.wpn_fps_upg_s_devgru.a_obj="a_s_geronimo"
end
if self.parts.wpn_fps_rsass_stock then
    self.wpn_fps_ass_rsass.override.wpn_fps_rsass_stock.a_obj="a_s_rsass"
end
if self.parts.wpn_fps_upg_m4_s_ubr then
    self.wpn_fps_ass_rsass.override.wpn_fps_upg_m4_s_ubr = {a_obj="a_s_m4ubr"}
end
if self.parts.wpn_fps_upg_m4_s_ubr_col then
    self.wpn_fps_ass_rsass.override.wpn_fps_upg_m4_s_ubr_col = {a_obj="a_s_m4ubr_col"}
end
if self.parts.wpn_fps_rsass_handguard then
    self.wpn_fps_ass_m4.override.wpn_fps_rsass_handguard = {a_obj="a_s_m4_rsasshandguard"}
end
if self.parts.wpn_fps_rsass_handguard then
    self.wpn_fps_ass_m16.override.wpn_fps_rsass_handguard = {a_obj="a_s_m4_rsasshandguard"}
end
local custom_weapon_id = "wpn_fps_ass_rsass"
local stance_base_id = "wpn_fps_snp_tti"
local uses_sights = {"wpn_fps_upg_o_specter","wpn_fps_upg_o_aimpoint","wpn_fps_upg_o_docter","wpn_fps_upg_o_eotech","wpn_fps_upg_o_t1micro","wpn_fps_upg_o_acog","wpn_fps_upg_o_cmore","wpn_fps_upg_o_aimpoint_2","wpn_fps_upg_o_eotech_xps","wpn_fps_upg_o_reflex","wpn_fps_upg_o_rx01","wpn_fps_upg_o_rx30","wpn_fps_upg_o_cs","wpn_fps_upg_o_shortdot","wpn_fps_upg_o_leupold","wpn_fps_upg_o_45iron","wpn_fps_upg_o_45rds","wpn_fps_upg_o_spot","wpn_fps_upg_o_xpsg33_magnifier","wpn_fps_upg_o_45rds_v2"}
--
for _, sight_id in pairs(uses_sights) do
	if self.parts[sight_id].stance_mod[stance_base_id] then
		self.parts[sight_id].stance_mod[custom_weapon_id] = deep_clone(self.parts[sight_id].stance_mod[stance_base_id])
	else
		log("[ERROR] " .. custom_weapon_id .. " Missing stance_mod data for: " .. sight_id, stance_base_id)
	end
end
--self.parts.wpn_fps_rsass_ironsight.stance_mod.wpn_fps_ass_rsass.rotation = Rotation(0.07, -0.165, 0) -- Dont ask
end)